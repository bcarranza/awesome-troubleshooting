# awesome-troubleshooting

> A collection of resources for general troubleshooting and debugging. Quality over quantity.

## Certificates

  - [Let's Debug](https://letsdebug.net) - Let's Debug is a diagnostic tool/website to help figure out why you might not be able to issue a certificate for Let's Encrypt™.
  - [Usable X.509 errors: OpenSSL](https://x509errors.org/) - [Their] goal is to simplify the ecosystem by consolidating the errors and their documentation (similarly to web documentation) and better explaining what the validation errors mean.

## DNS

  - [DNS Propagation Check](https://dnsmap.io/) - Provides free dns lookup service for checking domain name server records against a randomly selected list of DNS servers in different corners of the world.
  - [dns.computer](https://www.dns.computer/)
  - [dnsdumpster](https://dnsdumpster.com/) - dns recon & reserch, find & lookup dns records

## General

  - [webhook.site](https://webhook.site) - **Webhook.site** lets you easily inspect, test and automate (with the visual [Custom Actions builder](https://docs.webhook.site/custom-actions.html), or [WebhookScript](https://docs.webhook.site/webhookscript.html)) any incoming HTTP request or e-mail.

## Networking
  - [iperf](https://iperf.fr/)
  - [Traceroute Tool from KeyCDN](https://tools.keycdn.com/traceroute) - Get `traceroute` results from hosts in multiple geographic regions to the host you specify.

## SSH

  - [Rebex SSH Check](https://sshcheck.com/)
  
